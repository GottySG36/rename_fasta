// Utility to convert fasta headers to something simple to avoid error with certains tools

package main

import (
    "flag"
    "fmt"
    "log"
    "os"

    "bitbucket.org/GottySG36/sequtils"
)

var (
    inp     = flag.String("i",      "",     "Input fasta/q file to read.")
    out     = flag.String("o",      "",     "Output prefix to use for naming files.")

    prepend = flag.Bool("prepend",  false,  "Tells this tool to add a prefix to the existing headers.")
    appnd   = flag.Bool("append",   false,  "Tells this tool to add a suffix to the existing headers.")
    simple  = flag.Bool("simplify", false,  "Rename sequences header with simpler ones.")

    header  = flag.String("head",   "",     "New information used for renaming sequences.")
    sep     = flag.String("d",      "|",    "Delimiter used to delimit fields in fasta header.")

    restore = flag.String("r-file", "",    "Instead of replacing the header names, will use whats found in `<restore-file>`.")
    flip    = flag.Bool("r-flip", false,   "Specifies that we wish to reverse the columns in restore file. Allows the use of a restore file to go back and forth.")

    noConv  = flag.Bool("no-conv", false, "Will not write the converter. Useful if you don't need to revert names at some point.")

    synopsis = `Simple utility to read in a fasta file and manipulate its sequences' header.`

    uString  = `rename_fasta -i <FILE> -o <FILE> -header <STR> [-d STR] {-append, -prepend, -simplify}`
    details  = `This utility is designed to read in a fasta file and replace the existing headers entirely or by
either prepending or appending extra information.

The old (column 1) and new names (column 2) are then recorded in a file for possible later use.`
)

var Usage = func() {
    fmt.Fprintf(flag.CommandLine.Output(), "%v\n\n", synopsis)
    fmt.Fprintf(flag.CommandLine.Output(), "Usage : %v\n\n", uString)

    fmt.Fprintf(flag.CommandLine.Output(), "Arguments of %s:\n", os.Args[0])
    flag.PrintDefaults()

    fmt.Printf("\nDetailed description :\n%v\n", details)
}

func main() {
    flag.Usage = Usage
    flag.Parse()

    // Makes sure we receive one and only one modifying flag
    if *restore == "" {
        if !((*prepend && !*appnd && !*simple) || (!*prepend && *appnd && !*simple) || (!*prepend && !*appnd && *simple)) {
            log.Fatalf("Error -:- You must use one and only one of '-prepend', '-append' and '-simplify")
        }

        if *header == "" {
            log.Fatalf("Error -:- You need to provide the new header information with '-head'.\n")
        }
    }

    // Load fasta file
    fna, err := sequtils.LoadFasta(*inp)
    if err != nil {
        log.Fatalf("Error -:- LoadFasta : %v\n", err)
    }
    var c sequtils.Converter
    switch {
    case *prepend:
        c, err = fna.AddHeaderPrefix(*header, *sep)
        if err != nil {
            log.Fatalf("Error -:- AddHeaderPrefix : %v\n", err)
        }

    case *appnd:
        c, err = fna.AddHeaderSuffix(*header, *sep)
        if err != nil {
            log.Fatalf("Error -:- AddHeaderSuffix : %v\n", err)
        }

    case *simple:
        c, err = fna.ReplaceHeader(*header)
        if err != nil {
            log.Fatalf("Error -:- ReplaceHeader : %v\n", err)
        }
    case *restore != "":
        c, err := sequtils.LoadConverter(*restore, *flip)
        if err != nil {
            log.Fatalf("Error -:- LoadConverter : %v\n", err)
        }
        err = fna.HeaderFromConverter(c)
        if err != nil {
            log.Fatalf("Error -:- HeaderFromConverter : %v\n", err)
        }
    }

    fna.Write(fmt.Sprintf("%v.fna", *out))
    if !*noConv && *restore == "" {
        c.ToCsv(fmt.Sprintf("%v-headers.csv", *out))
    }
}
